import { async } from '@angular/core/testing';
import { MapsService } from './../services/maps.service';
import { Component, OnInit } from '@angular/core';
import { JsonPipe } from '@angular/common';



@Component({
  selector: 'app-map-path',
  templateUrl: './map-path.component.html',
  styleUrls: ['./map-path.component.css']
})
export class MapPathComponent implements OnInit{

  testlat:number=6.813873;
  testlong:number= 80.019309;
 markers:any;
 direction:any[];

 public lat:string;
 public lng:string;

 public origin:any;
 public destination:any;

  constructor(private maps:MapsService) { 


      this.maps.getStations().subscribe(results =>{

        this.markers=results;
        this.getMarkers(this.markers);
        
      })

      // console.log(this.markers);
      

  }

  ngOnInit(){

   
    // this.getDirections();
   

  }


  getDirections(){

    
    this.origin={ lat:7.048875,lng:79.89662 }
    this.destination={ lng:79.89666,lat:7.05749} 
    console.log(this.origin,this.destination)
      
    

  }


  getMarkers(markers){

    let a;
    let i;
    a=markers.length;
    console.log(markers);
    for(i=0;i<=a;i++)
    {
      this.origin= { lat:markers[0].latitude,lng:markers[0].longitude}
      let b=i+1;
      this.destination= { lng:markers[b].longitude,lat:markers[b].latitude }  
       
        // console.log(this.origin)
        // console.log(this.destination) 
        // console.log(i)  

    }

  }

  
}
