import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {RouterModule,Routes} from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { NavigationComponent } from './myComponents/navigation/navigation.component';
import { AboutComponent } from './myComponents/about/about.component';
import { FAQComponent } from './myComponents/faq/faq.component';
import { ContactComponent } from './myComponents/contact/contact.component';
import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction' ;
import { MapPathComponent } from './map-path/map-path.component';

const applicationRoutes:Routes = [
  {path:'about',component:AboutComponent},
  // {path:'loadmap',component:MapComponent},
  {path:'faq',component:FAQComponent},
  {path:'contactus',component:ContactComponent},
  {path:'map', component:MapPathComponent}
  
];

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    AboutComponent,
    // MapComponent,
    FAQComponent,
    ContactComponent,
    MapPathComponent,
    
  ],
  imports: [
    BrowserModule,
    AgmDirectionModule,
    HttpClientModule,
    RouterModule.forRoot(applicationRoutes),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCR2wJok3BAszZGMJVKyleL9YJUq9rhp2E'
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
