import { HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class MapsService {

  uri = 'http://localhost:8081';

  constructor(private http:HttpClient) { 

  }


  getStations(){

    return this.http.get(`${this.uri}/getRouteUpdates`);
    

  }

}
