var sequelize = require('sequelize');

var dbSeq = new sequelize('mysql://localhost:3306/public_transport_information_system');

var User = sequelize.define('users',
{
	userID:
	{
		type:		sequelize.String,
		unique:		true,
		allowNull:	false
	},
	email:
	{
		type:		sequelize.String,
		unique:		true,
		allowNull:	false
	},
	uPassword:
	{
		type:		sequelize.String,
		allowNull:	false
	},
	mobileNo:
	{
		type:		sequelize.String,
		unique:		true,
		allowNull:	false
	}
});

module.exports = User;