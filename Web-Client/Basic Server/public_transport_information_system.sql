-- phpMyAdmin SQL Dump
-- version 4.5.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 29, 2018 at 05:32 AM
-- Server version: 5.7.11
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `public_transport_information_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `dynamicroute`
--

CREATE TABLE `dynamicroute` (
  `dynamicID` int(10) NOT NULL,
  `RSID` char(4) DEFAULT NULL,
  `arrivalTime` time DEFAULT NULL,
  `userID` char(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dynamicroute`
--

INSERT INTO `dynamicroute` (`dynamicID`, `RSID`, `arrivalTime`, `userID`) VALUES
(1, '13', '08:17:05', '1'),
(2, '15', '03:17:11', '2');

-- --------------------------------------------------------

--
-- Table structure for table `route`
--

CREATE TABLE `route` (
  `routeID` int(100) NOT NULL,
  `routeNo` int(11) DEFAULT NULL,
  `type` varchar(200) DEFAULT NULL,
  `startLocation` varchar(100) DEFAULT NULL,
  `endLocation` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `route`
--

INSERT INTO `route` (`routeID`, `routeNo`, `type`, `startLocation`, `endLocation`) VALUES
(27, 2, '1', '1', '1'),
(28, 3222, '1', '1', '1'),
(30, 240, 'Bus', 'colombo', 'negombo'),
(31, 12888, 'Bus', 'Kasbawa', 'Colombo'),
(32, 138, 'Bus', 'Kadawatha', 'Colombo Fort');

-- --------------------------------------------------------

--
-- Table structure for table `route_station`
--

CREATE TABLE `route_station` (
  `rsID` int(100) NOT NULL,
  `stID` int(100) NOT NULL,
  `routeID` int(100) NOT NULL,
  `order` int(100) NOT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `route_station`
--

INSERT INTO `route_station` (`rsID`, `stID`, `routeID`, `order`, `latitude`, `longitude`) VALUES
(13, 1, 30, 4, 7.06900975878916, 79.893864088079),
(14, 2, 30, 3, 7.062429691435711, 79.89410012247231),
(15, 3, 30, 2, 7.057499903249518, 79.89666431429055),
(16, 4, 30, 1, 7.048875309521122, 79.89662139894631);

-- --------------------------------------------------------

--
-- Table structure for table `staticroute`
--

CREATE TABLE `staticroute` (
  `staticID` char(10) NOT NULL,
  `RSID` char(4) DEFAULT NULL,
  `arrivalTime` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `station`
--

CREATE TABLE `station` (
  `stationID` int(100) NOT NULL,
  `stationName` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `station`
--

INSERT INTO `station` (`stationID`, `stationName`) VALUES
(1, 'Weligampitiya'),
(2, 'Kapuwaththa'),
(3, 'Rilaulla'),
(4, 'Kandana');

-- --------------------------------------------------------

--
-- Table structure for table `stations`
--

CREATE TABLE `stations` (
  `stationID` int(10) NOT NULL,
  `stationName` char(50) DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stations`
--

INSERT INTO `stations` (`stationID`, `stationName`, `latitude`, `longitude`) VALUES
(1, 'Kandana', 7.048875309521122, 79.89662139894631),
(2, 'Rilaulla', 7.057499903249518, 79.89666431429055),
(3, 'Kapuwatta', 7.062429691435711, 79.89410012247231),
(4, 'Weligampitiya', 7.06900975878916, 79.893864088079);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userID` char(10) NOT NULL,
  `email` char(50) DEFAULT NULL,
  `uPassword` char(20) DEFAULT NULL,
  `mobileNo` char(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dynamicroute`
--
ALTER TABLE `dynamicroute`
  ADD PRIMARY KEY (`dynamicID`);

--
-- Indexes for table `route`
--
ALTER TABLE `route`
  ADD PRIMARY KEY (`routeID`);

--
-- Indexes for table `route_station`
--
ALTER TABLE `route_station`
  ADD PRIMARY KEY (`rsID`);

--
-- Indexes for table `staticroute`
--
ALTER TABLE `staticroute`
  ADD PRIMARY KEY (`staticID`);

--
-- Indexes for table `station`
--
ALTER TABLE `station`
  ADD PRIMARY KEY (`stationID`);

--
-- Indexes for table `stations`
--
ALTER TABLE `stations`
  ADD PRIMARY KEY (`stationID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dynamicroute`
--
ALTER TABLE `dynamicroute`
  MODIFY `dynamicID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `route`
--
ALTER TABLE `route`
  MODIFY `routeID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `route_station`
--
ALTER TABLE `route_station`
  MODIFY `rsID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `station`
--
ALTER TABLE `station`
  MODIFY `stationID` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `stations`
--
ALTER TABLE `stations`
  MODIFY `stationID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
