var express = require('express');
var bodyParser = require('body-parser');
var mysql = require('mysql');
var session = require('express-session');
var cookieParser = require('cookie-parser');
//var User = require('./User');

var app = express();

app.use(express.static('public'));
app.use(bodyParser.urlencoded( {extended: true} ));
app.use(bodyParser.json());

app.use(cookieParser());

app.use(session(
{
	key: 'user_sid',
	secret: 'crowd_pub_transport',
	resave: false,
	saveUninitialized: false,
	cookie:
	{
		expires: 600000
	}
}));

var sessionChecker = function(req, res, next)
{
	if (req.session.user && req.cookies.user_sid)
	{
		res.redirect('/');
	}
	else
	{
		next();
	}
};

var con = mysql.createConnection(
{
	host:	"localhost",
	user:	"root",
	password:"root",
	port:	3306,
	database:	"public_transport_information_system"
});

con.connect(function(err)
{
	if(err)
		console.log(err);
	else
		console.log("Connected to the Public Transport database!");
});


app.get('/', function(req, res)
{
	if (req.session.user && req.cookies.user_sid)
	{
		res.sendFile(__dirname + "/Main.html");
	}
	else
	{
		res.redirect('/login');
	}
});

app.get('/login', function(req, res)
{
	res.sendFile(__dirname + "/Login.html");
});

//Accepts and handles POST messages from the login HTML page.
app.post('/login_func', function(req, res)
{
	var email = req.body.email, passwd = req.body.passwd;
	
	con.query("SELECT * FROM Users", function(err, rows)
	{
		var found = false;
		if(err)
			throw err;
		
		for(var i in rows)
		{
			if(rows[i].email == email && rows[i].uPassword == passwd)
			{
				found = true;
				req.session.user = email;
				res.redirect('/');
				break;
			}
		}
		if (found == false)
			res.send("<title>Login failed</title>That username and/or password does not exist in the database.");
	});
});

//Accepts and handles POST messages from the registration HTML page.
app.post('/registration_func', function(req, res)
{
	con.query("SELECT * FROM Users", function(err, rows)
	{
		var email = req.body.email, passwd = req.body.passwd, mobileno = req.body.mobileno;
		var found = false;
		
		if(err)
			throw err;
		for(var i in rows)
		{
			if(rows[i].email == email)
			{
				found = true;
				res.send('<title>Existing email</title>That email already exists in the database.');
				break;
			}
		}
		
		if(found == false)
		{
			con.query("SELECT COUNT(*) AS 'count' FROM Users", function(err, rows)
			{
				if(err)
					throw err;
				var count = rows[0].count;
				
				var id = "U";
				for(var i = 0; i < 9-String(count).length; i++)
					id += "0";
				id += count;
				
				con.query("INSERT INTO Users VALUES ('"+id+"', '"+email+"', '"+passwd+"', '"+mobileno+"')", function(err)
				{
					if(err)
						throw err;
					res.send('<title>Registration successful</title>Registration was successful. Welcome to our system!');
				});
			});
		}
	});
});

app.post('/ins_function', function(req, res)
{
	con.query("SELECT * FROM Users", function(err, rows)
	{
		var email = req.body.email, passwd = req.body.passwd, mobileno = req.body.mobileno;
		var found = false;
		
		if(err)
			throw err;
	
		con.query("INSERT INTO `public_transport_information_system`.`route` (`routeNo`, `type`, `startLocation`, `endLocation`) VALUES ('20', '2', NULL, NULL)", function(err)
		{
			if(err)
				throw err;
			res.send('<title>Registration successful</title>Inserted!');
		});


	});
});

app.post('/addroute', function(req, res)
{
	//console.log(req);
	con.query("SELECT * FROM Users", function(err, rows)
	{
		var rs = req.body.rs, rn = req.body.rn, re = req.body.re, rt = req.body.rt;
		var found = false;
		
		if(err)
			throw err;
	
		con.query("INSERT INTO `route`(`routeNo`, `type`, `startLocation`, `endLocation`) VALUES ('"+rn+"', '"+rt+"', '"+rs+"', '"+re+"')", function(err)
		{
			if(err)
				throw err;
			res.send('<title>Registration successful</title>Inserted!');
		});


	});
});

app.get('/getroutes', function(req, res)
{
	//var email = req.body.email, passwd = req.body.passwd;
	
	con.query("SELECT * FROM route", function(err, rows)
	{
		if(err)
		{
			res.send("Error");
		}
		else
		{
			res.send(	rows);
		}
	});
});


app.get('/ins', function(req, res)
{
	res.sendFile(__dirname + "/ins.html");
});

app.get('/registration', function(req, res)
{
	res.sendFile(__dirname + "/Registration.html");
});




//chaithika
app.get('/getStationsList', function(req, res)
{
	con.query("SELECT * FROM stations", function(err, rows)
	{
		
		if(err){
			res.send("Invalid");
		}else{
			res.send(rows);
			
		}
	})
})

app.get('/getStationList', function (req, res) {
	con.query("SELECT s.stationName, rs.latitude, rs.longitude, rs.rsID FROM route r, station s, route_station rs WHERE r.routeNo=240 AND rs.routeID=r.routeID AND rs.stID=s.stationID ORDER BY rs.order ASC", function (err, rows) {
		res.header("Access-Control-Allow-Origin", "*");
res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
		if (err) {
			res.send("Invalid");
		} else {
			res.send(rows);
		}
	})
})

//new method
app.get('/getRouteUpdates', function (req, res) {
	var routeID = 32;
	con.query("select RS.rsID as 'RSID', RS.latitude, RS.longitude, CAST(MAX(TT.ArrivalTime) as time) as 'ArrivalTime' from time_table TT, route_station RS where TT.RSID = RS.rsID and TT.ArrivalTime < CAST(NOW() as time) and RS.routeID = '" + routeID + "' and TT.RSID NOT IN(select RSID from dynamic_route) group by TT.rsID UNION select RS.rsID as 'RSID', RS.latitude, RS.longitude, CAST(MAX(DR.arrivalTime) as time) as 'ArrivalTime' from dynamic_route DR, route_station RS where RS.rsID = DR.RSID and RS.routeID = '" + routeID +"' group by DR.RSID order by RSID", function (err, rows) {
		res.header("Access-Control-Allow-Origin", "*");
res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
		if (err) {
			res.send("Invalid");
		} else {
			res.send(rows);
		}
	})
})

app.get('/getDynamicUpdates', function (req, res) {
	con.query("SELECT dr.arrivalTime, rs.rsID FROM dynamicroute dr, route_station rs WHERE rs.rsID = dr.RSID ", function (err, rows) {
		if (err) {
			res.send("Invalid");
		} else {
			res.send(rows);
		}
	})
})

app.post('/addStations',function(req,res)
{
	var stname = req.body.stname;
	var stlat = req.body.stlat;
	var stlng = req.body.stlng;

	// con.query("INSERT INTO `route`(`routeNo`, `type`, `startLocation`, `endLocation`) VALUES ('" + rn + "', '" + rt + "', '" + rs + "', '" + re + "')", function (err) {
	con.query("INSERT INTO `stations`(`stationID`, `stationName`, `latitude`, `longitude`) VALUES (null,'" +stname+ "','" +stlat+ "','" +stlng+ "')", function (err){	
		if (err)
			res.send(err);
		res.send('<title>Addition successful</title>Inserted!');
	})

})

app.get('/loadmap', function(req,res)
{
	res.sendFile(__dirname + "/loadmap.html");
})

app.get('/loadmap111', function (req, res) {
	res.sendFile(__dirname + "/loadmap111.html");
})
app.get('/loadmap12', function (req, res) {
	res.sendFile(__dirname + "/loadmap12.html");
})



var server = app.listen(8081, function()
{
   var host = server.address().address;
   var port = server.address().port;
   console.log("Server listening at http://%s:%s", host, port);
});